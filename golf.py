#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
TMCS golf game, written in pygame.

=====
Usage
=====
You need pygame. See the INSTALL_PYGAME.rst file.

Then download the frames::
  ./golf.py download

Then run this script::
  ./golf.py play

The "frames" are the images of the molecule at each point in phase space. You 
need to render these (using `./golf.py render`) or download them from tims 
website (using `golf.py download`) before playing the game.

========
Commands
========
./golf.py render
  Use jmol to render the frames. This is only useful if you have jmol.

./golf.py download
  Download the frames instead of rendering them.

./golf.py play
  Play the game

./golf.py playno
  Play the game without the molecule. This makes loading much faster and is 
  useful for testing.

"""

from __future__ import division
import pygame
import numpy
import subprocess
import sys
import os
N = 50
x = numpy.linspace(0,1,N)
y = numpy.linspace(0,1,N)
xvals = numpy.linspace(0,4,N )
yvals = numpy.linspace(0,4,N )
DISPLAYWHENRENDER = True

molwidth = 480
molheight = 480

##########################################################################
##########################################################################
##########################################################################

def cleanframesdir():
  """clear out or create the frames directory"""
  if not os.path.exists("frames"):
    os.makedirs("frames")
  for the_file in os.listdir("frames"):
    file_path = os.path.join("frames", the_file)  
    if os.path.isfile(file_path):
      os.unlink(file_path)

##########################################################################
##########################################################################
##########################################################################

def renderframes():
  """Use jmol to render the molecules. Store the results in the "frames" 
  folder. The game then loads the images from the folder."""

  cleanframesdir()
  
  pointtemplate = """
  translateSelected {{ {xval} ,{yval},0 }};
  write IMAGE 480 480 PNG 100 "{fn}";
  translateSelected {{ {negxval} ,{negyval},0 }};
  """

  totalpoints = []
  for i in range(N):
    for j in range(N):
      fn = os.path.join("frames", "out_{:03}{:03}.png".format(i,j))
      point = pointtemplate.format(xval=xvals[i],yval=yvals[j],
                                  negxval=-xvals[i],negyval=-yvals[j],
                      fn= fn   )
      totalpoints.append (point) 

  totalpoints = "\n".join(totalpoints)
  scriptname = os.path.join("frames","jmolplotter_full.jmol") 
  open(scriptname ,"w").write(
          open("jmolplotter_pythonconnect.jmol").read() + totalpoints )

  print "RUNNING JMOL"
  cmd = ( ["jmol", "-s"] 
                        + ( ["-n"] if not DISPLAYWHENRENDER else [])
                        + [ scriptname ] )
  print "running", cmd
  subprocess.check_call( cmd )
  
  print "jmol exited successfully."

##########################################################################
##########################################################################
##########################################################################

def downloadframes():
  """If you dont have jmol, download the frames from tims website."""

  cleanframesdir()

  print "Downloading frames ...."

  # ----------------- Download the file with progress bar ---------------
  import urllib2
  url = "http://community.dur.ac.uk/timothy.wiles2/tmcsgolf/frames.tar"
  file_name = "frames.tar"
  u = urllib2.urlopen(url)
  with open("frames.tar", 'wb') as f:
    meta = u.info()
    file_size = int(meta.getheaders("Content-Length")[0])
    print "Downloading: %s Bytes: %s" % (file_name, file_size)

    file_size_dl = 0
    block_sz = 8192
    while True:
        buffer = u.read(block_sz)
        if not buffer:
            break

        file_size_dl += len(buffer)
        f.write(buffer)
        status = r"%10d  [%3.2f%%]" % (
                      file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8)*(len(status)+1)
        print status,
  # --------------end Download the file with progress bar ---------------

  print "Download complete. Untarring into frames."
  import tarfile
  tarfile.open(  "frames.tar").extractall()
  

##########################################################################
##########################################################################
##########################################################################  

def main(showframes=True):
    """Play the golf game. If showframes=True, then render the molecule in the
    right hand pain."""
    
    # -------------- initialize the pygame module ------------------------
    pygame.init()
    clock = pygame.time.Clock()

    pygame.display.set_caption("TMCS golf game")
    
    # create a surface on screen 
    windowSurfaceObj = pygame.display.set_mode((2*molwidth,molheight))
    # -----------end initialize the pygame module ------------------------

    def listenforexit(events):
      for event in events:
        if event.type == pygame.QUIT:
            # change the value to False, to exit the main loop
            pygame.quit()
            sys.exit()


    # ===================================================================
    # ======================== Load the images ==========================
    windowSurfaceObj.fill(pygame.Color(0,0,0))
    loadingmsg = pygame.font.Font('freesansbold.ttf',32)\
                  .render("loading frames...",True,pygame.Color(255,255,255))
    rect = loadingmsg.get_rect()    
    rect.topleft=(  10,20)
    windowSurfaceObj.blit(loadingmsg,rect)

    instrmsg = [  
    "Instructions: ",
    "Press the arrow keys to move the red golf ball around phase space",
    "Press 1-9 to change the speed",
    "Press the uparrow to start"]
    instrmsg = [  pygame.font.Font('freesansbold.ttf',24)
                  .render(line,True,(255,255,255)) for line in instrmsg  ]
    for i,line in enumerate(instrmsg):
      if i!=len(instrmsg)-1:
        rect = line.get_rect()
        rect.topleft = 10,150+26*i   
        windowSurfaceObj.blit(line,rect)         
    
    pygame.display.update()

    # ...................... draw progress bar ............................
    barleft, bartop, barwidth, barheight = 10, 100, 400,30
    barcolor = (255,255,255)
    backcolor = (0,0,0)
    def updatebar(progress):
      pygame.draw.rect(windowSurfaceObj, backcolor,
                 pygame.Rect(barleft, bartop, barwidth, barheight))
      #frame:
      pygame.draw.rect(windowSurfaceObj, barcolor,
                 pygame.Rect(barleft, bartop, barwidth, barheight),1) 
      #bar:
      pygame.draw.rect(windowSurfaceObj, barcolor,
                   pygame.Rect(barleft, bartop, barwidth*progress, barheight))
      pygame.display.update()
    # ...................end draw progress bar ............................

    # ................... actually load the images ........................
    if showframes:
      frames = {}
      for i in range(N):
        listenforexit(pygame.event.get())
        updatebar(i/N)
        for j in range(N):
          fn = os.path.join("frames", "out_{:03}{:03}.png".format(i,j))
          frames[i,j] = pygame.image.load(fn ).convert()
    updatebar(1)
    # ................end actually load the images ........................

    for i,line in enumerate(instrmsg):
      if i==len(instrmsg)-1:
        rect = line.get_rect()
        rect.topleft = 10,150+26*i   
        windowSurfaceObj.blit(line,rect)         
    
    pygame.display.update()

    # .......... Wait for the user to press the up arrow .................
    pressed=False
    while not pressed:
      # event handling, gets all event from the eventqueue
      events = pygame.event.get()
      listenforexit(events)
      for event in events:
          if event.type==pygame.KEYUP:
              if   event.key == pygame.K_UP:
                pressed=True
    # ...... end Wait for the user to press the up arrow .................

    # =====================end Load the images ==========================    
    # ===================================================================



    # -------------- make the phase space point --------------------------
    phasespacepoint = pygame.Surface([10, 10])
    RED = ( 255, 0, 0)
    phasespacepoint.fill(RED)
    phasespacepointsprite = pygame.sprite.Sprite()
    phasespacepointsprite.image = phasespacepoint
    phasespacepointsprite.rect = phasespacepoint.get_rect()   
    movingsprites = pygame.sprite.Group()
    movingsprites.add(phasespacepointsprite)
    # -----------end make the phase space point --------------------------

    # ===================================================================
    # ===================== Main loop ===================================    
    speed=1
    pos = [0,0]
    vel = [0,0]

    while True:
        # event handling, gets all event from the eventqueue
        events = pygame.event.get()
        listenforexit(events)
        for event in events:
            if event.type==pygame.KEYDOWN:
                if   event.key == pygame.K_UP:
                  vel[1] -= speed
                elif event.key == pygame.K_DOWN:
                  vel[1] += speed
                elif event.key == pygame.K_LEFT:
                  vel[0] -=speed 
                elif event.key == pygame.K_RIGHT:
                  vel[0] +=speed 
            elif event.type==pygame.KEYUP:
                if   event.key == pygame.K_UP:
                  vel[1] += speed
                elif event.key == pygame.K_DOWN:
                  vel[1] -= speed
                elif event.key == pygame.K_LEFT:
                  vel[0] +=speed 
                elif event.key == pygame.K_RIGHT:
                  vel[0] -=speed 
                elif event.key in range(pygame.K_1,pygame.K_9+1): 
                  # We've hit a number key
                  speed = event.key - pygame.K_1 +1

        # Move the piece
        pos[1] = (pos[1]+vel[1]) % N
        pos[0] = (pos[0]+vel[0]) % N

        # clear the screen
        windowSurfaceObj.fill(pygame.Color(0,0,0))

        # Render the molecule
        if showframes:
          windowSurfaceObj.blit( frames[pos[0],pos[1]] ,(molwidth,0))

        # Render the phase space point  
        phasespacepointsprite.rect.x = pos[0]/N *molwidth  
        phasespacepointsprite.rect.y = pos[1]/N *molwidth  
        movingsprites.draw(windowSurfaceObj)



        # Update the screen
        #pygame.transform.scale2x(windowSurfaceObj,windowSurfaceObj)
        pygame.display.update()
        clock.tick(30)

    # ==================end Main loop ===================================    
    # ===================================================================

##########################################################################
##########################################################################
##########################################################################  

# ====================== Parse commands =================================

if len(sys.argv)!=2:
  raise ValueError("You need to specify a command.")

if sys.argv[1]=="render":
  renderframes()
elif sys.argv[1]=="download":
  downloadframes()
elif sys.argv[1]=="play":
  main()
elif sys.argv[1]=="playno":
  main(False)
else:
  raise ValueError(sys.argv[1] + " Not a valid command")

# ===================end Parse commands =================================

