The main file is golf.py. See the top of golf.py for usage information.

golf.py is a game where you move the golf ball around phase space with the 
arrow keys. An image of a molecule updates as you do this!