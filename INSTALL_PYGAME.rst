Run the following to get pygame::
  su install
  brew update
  brew install gcc
  brew install pygame
  sudo su
  echo 'import site; site.addsitedir("/usr/local/lib/python2.7/site-packages")' >> /Library/Python/2.7/site-packages/homebrew.pth

The last line adds /usr/local/lib/python2.7/site-packages to your python site 
path. This is very useful!

